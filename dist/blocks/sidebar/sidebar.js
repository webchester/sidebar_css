const sidebar = document.querySelector('.sidebar');
const content = document.querySelector('.page__content');
const widthPageWhenIsLoaded = window.innerWidth;
if (widthPageWhenIsLoaded < 1200) {
    sidebar.classList.add('sidebar--collapsed');
    content.classList.add('page__content--full-width');
}
window.addEventListener('resize', (e) => {
    if (e.currentTarget.innerWidth <= 1200) {
        sidebar.classList.add('sidebar--collapsed');
        content.classList.add('page__content--full-width');
    } else {
        sidebar.classList.remove('sidebar--collapsed');
        content.classList.remove('page__content--full-width');
    }
});


function sideBarToggle() {
    sidebar.classList.toggle('sidebar--collapsed');
    content.classList.toggle('page__content--full-width');
}